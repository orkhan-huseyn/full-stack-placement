import React, { useEffect } from 'react';
import axios from 'axios';

const baseUrl = process.env.REACT_APP_BACKEND_URL;

function App() {
  useEffect(() => {
    axios
      .get(`${baseUrl}/ping`)
      .then((r) => r.data)
      .then(console.log);
  }, []);

  return (
    <h1>
      Hello, from <code>App.js</code>
    </h1>
  );
}

export default App;
