const http = require('http');
const app = require('./app');

const PORT = process.env.PORT || 8080;

const httpServer = http.createServer(app);

httpServer.listen(PORT, () => {
  console.log(`HTTP Server is running on port ${PORT}`);
});
