require('dotenv').config();

const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');

mongoose.connect(process.env.DB_CONNECTION_STRING);

const app = express();

app.use(cors());

app.get('/ping', (req, res) => {
  res.json({ message: 'pong' });
});

module.exports = app;
