# Full-Stack Placement Test

Welcome to full-stack placement test. This is just a small task for you to prove your full-stack development skills and determine your level.
This task will test your skills on:

- Front-end development with React
- API design and development with Express
- Database design and development with Mongoose

## Getting started

The application is already set up for you to save you a little time for getting started.
Following are the steps you need to do to run the application in development mode:

1. Run `npm install` in root directory
2. Go to client directory (`cd client`) and run `npm install` there
3. Go to server directory (`cd server`) and run `npm install` there as well

Following command should do this for you:

```sh
npm install && cd client && npm install && cd ../server && npm install && cd ..
```

4. Run `npm run dev` in root directory. It will start an Express server on `http://localost:8080` and a React application on `http://localhost:3000`.
5. Test the client by opening browser and going to `http://localost:3000`.
6. Test the client by opening browser and going to `http://localost:8080/ping`. It should respond with `{"message":"pong"}` - it means everythin is fine;

## What should you do?

You will write a simple full-stack Todo Application.

### Back-end

A single Todo item should have following properties:

| Propery   | Type     |
| --------- | -------- |
| id        | ObjectId |
| title     | String   |
| completed | Boolean  |
| createdAt | Date     |

You will write a REST API with followin endpoints:

1. `GET /todos` should return list of all todos
2. `GET /todos/:id` should return single todo with given ID
3. `POST /todos` should insert a new item to database
4. `PUT /todos/:id` should update todo with corresponding ID in database
5. `DELETE /todos/:id` should delete todo item with given ID

You will use MongoDB as your primary database and Mongoose as ORM. Luckily, both are already set up for you inside `server/app.js`.
Open from `server/app.js` file and write your code there.

### Front-end

Write a single page React application which should look and behave like following:

![todo-app](./todo-app-final.gif)

You can use Bootstrap or any other CSS library to style your app.

## Grading

Your task will be graded as following:

You can score not more than 100%.

- Front-end part of the app will give you 35 points.
  - 7 points if listing todo items works
  - 7 points if adding new todo item works
  - 7 points if deleting todo item works
  - 14 points if updating todo item works
- Back-end part of the app will give you another 35 points.
  - 7 points if `GET /todos` endpoint works
  - 7 points if `POST /todos` endpoint works
  - 7 points if `GET /todos/:id` endpoint works
  - 7 points if `PUT /todos/:id` endpoint works
  - 7 points if `DELETE /todos/:id` endpoint works
- You will get another 30 points if your front-end and back-end works together, and each of the actions (add, delete, update etc.) are stored in database correctly.

## Exam rules

You are free to use Google to search and read documentation about Express, MongoDB, Mongoose or Bootstrap. Copying code from anyone or anywhere is strictly prohibited and you will be disqualified if we find any signature of plagiarism or cheating.
